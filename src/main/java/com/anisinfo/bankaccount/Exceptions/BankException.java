/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.bankaccount.Exceptions;

/**
 *
 * @author boulahia
 */
public class BankException extends Exception{
    
    public BankException(String errorMessage) {
        super(errorMessage);
    }
    
}
