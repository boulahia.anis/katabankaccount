/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.bankaccount;

import java.util.Date;

/**
 *
 * @author boulahia
 */
public class Operation {
    private Date operationDate;
    private TypeOperation operationType;
    private Double operationAmount;
    private Double newSolde;

    public Operation(TypeOperation type, Double amount, Double newSolde ) {
        this.operationDate = new Date();
        this.operationType = type;
        this.operationAmount = amount;
        this.newSolde = newSolde;
    }

    public TypeOperation getOperationType() {
        return operationType;
    }

    public void setOperationType(TypeOperation operationType) {
        this.operationType = operationType;
    }

    public Double getOperationAmount() {
        return operationAmount;
    }

    public void setOperationAmount(Double operationAmount) {
        this.operationAmount = operationAmount;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public Double getNewSolde() {
        return newSolde;
    }

    public void setNewSolde(Double newSolde) {
        this.newSolde = newSolde;
    }

    @Override
    public String toString() {
        return "operationDate : " + operationDate + " \t operationType : " + operationType + " \t operationAmount : " + operationAmount + " \t newSolde : " + newSolde;
    }

    
}
