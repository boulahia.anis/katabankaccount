/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.bankaccount.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author boulahia
 */
public class Utils {
   
     public static String formatDateFr(Date date) { //2011-12-31 00:00:00.000 --> 31/12/2011
        String dateFr = null;
        if (date != null) {
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            dateFr = formatter1.format(c.getTime());
        }
        return dateFr;
    }

}
