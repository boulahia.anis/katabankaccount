/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.bankaccount.impl;

import com.anisinfo.bankaccount.Exceptions.BankException;
import com.anisinfo.bankaccount.IAccount;
import com.anisinfo.bankaccount.Operation;
import com.anisinfo.bankaccount.TypeOperation;
import com.anisinfo.bankaccount.utils.Utils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author boulahia
 */
public class Account implements IAccount {

    private double solde;
    private List<Operation> listOperations = null;

    public Account() {
        this.solde = 0;
        listOperations = new ArrayList<>();
    }

    @Override
    public void depositMoney(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negative value not allowed in deposit operation");
        }
        this.solde += amount;
        Operation op = new Operation(TypeOperation.DEPOSIT, amount, this.solde);
        listOperations.add(op);
    }

    @Override
    public void retrieveMoney(double amount) throws BankException {
        if (this.solde - amount < 0) {
            throw new BankException("You don't have enough money to do this operation");
        }
        this.solde -= amount;
        Operation op = new Operation(TypeOperation.RETREIV, amount, getBalance());
        listOperations.add(op);
    }

    @Override
    public double getBalance() {
        return this.solde;
    }

    @Override
    public List<Operation> getListOperation() {
        return this.listOperations;
    }

    
    /**
     * this method permit to print operations list
     * @param args
     * @throws BankException 
     */
    
    public static void main(String[] args) throws BankException{
        Account account = new Account();
        account.depositMoney(100d);
        account.depositMoney(50d);
        account.retrieveMoney(80d);
        account.depositMoney(20d);

        printOperations(account.getListOperation());
    }

       public static void printOperations(List<Operation> listOp) {
        System.out.println("****************** opérations ******************** ");
        System.out.println("Date \t \t \t Type \t\t Amount \t New Solde");
        listOp.forEach(op -> {
            System.out.println(Utils.formatDateFr(op.getOperationDate()) + "\t" + op.getOperationType() + "\t\t" + op.getOperationAmount() + "\t\t" + op.getNewSolde());
        });
    }

}
