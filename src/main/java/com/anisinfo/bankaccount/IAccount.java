/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.bankaccount;

import com.anisinfo.bankaccount.Exceptions.BankException;
import java.util.List;

/**
 *
 * @author boulahia
 */
public interface IAccount {

    public void depositMoney(double amount); 
    
    public void retrieveMoney(double amount) throws BankException;
    
    public double getBalance(); 
    
    public List<Operation> getListOperation();
   
}
