/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anisinfo.bankaccount;

import com.anisinfo.bankaccount.Exceptions.BankException;
import com.anisinfo.bankaccount.impl.Account;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * @author boulahia
 */
public class AccountTest {

    private IAccount account;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        account = new Account();
    }

    @Test
    public void depositMoneyTest() {
        this.account.depositMoney(100d);
        assertEquals(100d, this.account.getBalance(), 0.0002);
    }

    @Test
    public void depositMoneyBadArgumentTest() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Negative value not allowed in deposit operation");
        this.account.depositMoney(-100d);
    }

    @Test
    public void retrieveMoneyTest() throws BankException {
        this.account.depositMoney(250d);
        this.account.retrieveMoney(200d);
        assertEquals(50, this.account.getBalance(), 0.0002);
    }

    @Test
    public void retrieveMoneyBadArgumentTest() throws BankException {
        thrown.expect(BankException.class);
        thrown.expectMessage("You don't have enough money to do this operation");
        this.account.retrieveMoney(600d);
    }

    @Test
    public void depositAndRetreiveSameTimeMoneyTest() throws BankException {
        this.account.depositMoney(100d);
        this.account.depositMoney(50d);
        this.account.retrieveMoney(100d);
        assertEquals(50d, this.account.getBalance(), 0.0002);
    }

    @Test
    public void getListOperationTest_ShouldHaveSizeThree() throws BankException {
        this.account.depositMoney(100d);
        this.account.depositMoney(50d);
        this.account.retrieveMoney(100d);
        assertEquals(3, this.account.getListOperation().size());
        assertTrue(50d == this.account.getListOperation().get(1).getOperationAmount());
        assertTrue(TypeOperation.DEPOSIT == this.account.getListOperation().get(1).getOperationType());
    }
}
